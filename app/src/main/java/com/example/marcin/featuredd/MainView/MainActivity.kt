package com.example.marcin.featuredd.MainView

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import com.example.marcin.featuredd.BuildConfig
import com.example.marcin.featuredd.R
import com.example.marcin.featuredd.base.BaseActivity
import org.opencv.android.OpenCVLoader
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : BaseActivity() {

    private val mFragment: MainFragment = MainFragment()
    private val mBottomTabAdapter: BottomTabAdapter = BottomTabAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pushFragments(mFragment.getInstance(), R.id.fragment_container, true, false, false)
        mBottomTabAdapter.prepareTab(tabs)
        if (BuildConfig.DEBUG)
            OpenCVLoader.initDebug()
    }

    fun onTabSelected(pos: Int) {

        when (pos) {

        }
    }


}
