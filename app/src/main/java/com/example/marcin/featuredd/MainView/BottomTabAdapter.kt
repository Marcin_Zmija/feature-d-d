package com.example.marcin.featuredd.MainView

import android.support.design.widget.TabLayout
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import com.example.marcin.featuredd.R

/**
 * Created by user on 15.05.2017.
 */

class BottomTabAdapter(private val context: MainActivity) {
    private val tabTitles: Array<String> = arrayOf("Home", "Last ride", "History", "About")
    private val imageResIdActive = intArrayOf(R.drawable.home_active, R.drawable.bicycle_active, R.drawable.history_active, R.drawable.info_active)
    private val imageResId = intArrayOf(R.drawable.home_ia, R.drawable.bicycle_ia, R.drawable.history_ia, R.drawable.info_ia)

    fun prepareTab(tabLayout: TabLayout) {
        tabLayout.addTab(tabLayout.newTab().setCustomView(setTabView(0)))
        tabLayout.addTab(tabLayout.newTab().setCustomView(unsetTabView(1)))
        tabLayout.addTab(tabLayout.newTab().setCustomView(unsetTabView(2)))
        tabLayout.addTab(tabLayout.newTab().setCustomView(unsetTabView(3)))

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                val img = tab.customView!!.findViewById<View>(R.id.category_iv) as ImageView
                img.setImageResource(getImageActive(tab.position))

                val tv = tab.customView!!.findViewById<View>(R.id.category_tv) as TextView
                tv.visibility = View.VISIBLE
                context.onTabSelected(tab.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                val img = tab.customView!!.findViewById<View>(R.id.category_iv) as ImageView
                img.setImageResource(getImage(tab.position))

                //                TextView tv = (TextView) tab.getCustomView().findViewById(R.id.category_tv);
                //                tv.setVisibility(View.GONE);
            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }

    fun selectTab(tab: TabLayout.Tab) {
        val img = tab.customView!!.findViewById<View>(R.id.category_iv) as ImageView
        img.setImageResource(getImageActive(tab.position))

        val tv = tab.customView!!.findViewById<View>(R.id.category_tv) as TextView
        tv.visibility = View.VISIBLE
    }

    fun unselectTab(tab: TabLayout.Tab) {
        val img = tab.customView!!.findViewById<View>(R.id.category_iv) as ImageView
        img.setImageResource(getImage(tab.position))
    }

    fun setTabView(pos: Int): View {
        val v = LayoutInflater.from(context).inflate(R.layout.adapter_bottom_tab, null)
        val mCategoryTv = v.findViewById<View>(R.id.category_tv) as TextView
        val mCategoryIv = v.findViewById<View>(R.id.category_iv) as ImageView
        if (pos >= 0 && pos < tabTitles.size) {
            mCategoryTv.text = tabTitles[pos]
            mCategoryIv.setImageResource(imageResIdActive[pos])
        }
        return v
    }

    fun getText(position: Int): String {
        return tabTitles[position]
    }

    fun getImageActive(position: Int): Int {
        return imageResIdActive[position]
    }


    fun getImage(position: Int): Int {
        return imageResId[position]
    }

    fun unsetTabView(pos: Int): View {
        val v = LayoutInflater.from(context).inflate(R.layout.adapter_bottom_tab, null)
        val mCategoryTv = v.findViewById<View>(R.id.category_tv) as TextView
        val mCategoryIv = v.findViewById<View>(R.id.category_iv) as ImageView
        mCategoryTv.text = tabTitles[pos]
        //        mCategoryTv.setVisibility(View.GONE);
        if (pos >= 0 && pos < imageResId.size) {
            mCategoryIv.setImageResource(imageResId[pos])
        }
        return v
    }
}
