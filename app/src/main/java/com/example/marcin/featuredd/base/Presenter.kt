package com.example.marcin.featuredd.base

/**
 * Created by user on 11.05.2017.
 */

interface Presenter<V : MvpView> {

    fun attachView(mvpView: V)

    fun detachView()
}
