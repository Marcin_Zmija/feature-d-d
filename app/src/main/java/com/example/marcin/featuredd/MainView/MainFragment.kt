package com.example.marcin.featuredd.MainView

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.util.Util
import com.example.marcin.featuredd.R
import kotlinx.android.synthetic.main.fragment_main.*
import org.greenrobot.eventbus.EventBus
import org.opencv.android.Utils
import org.opencv.core.Mat
import org.opencv.core.MatOfByte
import org.opencv.features2d.MSER
import org.opencv.imgproc.Imgproc
import org.opencv.photo.Photo
import java.io.*

/**
 * Created by marcin on 05.07.2017.
 */
class MainFragment : Fragment() {

    lateinit var mMainActivity: MainActivity
    private val REQUEST_EXTERNAL_STORAGE = 1
    private val SELECT_PICTURE = 1
    private val SELECT_PICTURE_2 = 2
    private var firstImage : Uri? = null
    private var secondImage : Uri? = null
    private var selectedImageUri : Uri? = null
    private var eventBus = EventBus.getDefault()
//    var outputImg = Mat()
//    var drawnMatches = MatOfByte()
    private val PERMISSIONS_STORAGE = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                              android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

    fun getInstance(): MainFragment {
        return MainFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_main, container, false)
        mMainActivity = activity as MainActivity
        verifyStoragePermissions(mMainActivity)
        return  view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    fun verifyStoragePermissions(activity: Activity) {
        val permission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            )
        }
    }

    fun compressImage(selectedImageUri: Uri) : Bitmap? {
        var selectedImagePath : String = getPath(selectedImageUri)
        val file = File(selectedImagePath)
            var bitmap : Bitmap? = null
                try {
                    bitmap = getBitmapFromUri(selectedImageUri);
                } catch (e: IOException) {
                   e.printStackTrace()
                    }
                    var os : OutputStream
                    try {
                        os = FileOutputStream(file)
                        bitmap?.compress(Bitmap.CompressFormat.JPEG, 80, os);
                        os.flush()
                        os.close()
                    } catch (e : Exception) {
                    }
        return  bitmap
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                selectedImageUri = data.data
                firstImage = selectedImageUri
                //                    Bitmap selectedImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                Glide.with(this)
                        .load(selectedImageUri)
                        .dontTransform()
                        .into(first_image)
            } else {
                selectedImageUri = data.data
                secondImage = selectedImageUri
//                compressImage(selectedImageUri as Uri)
                //                    Bitmap selectedImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                Glide.with(this)
                        .load(selectedImageUri)
//                        .atMost()
                        .into(second_image)
            }
                //                    mPhotoIv.setImageBitmap(getBitmapFromUri(selectedImageUri));
                //                filemanagerstring = selectedImageUri.getPath();
                //                    selectedImagePath = getPath(selectedImageUri);
                //                mAddImageSection.setVisibility(View.GONE);
                //                mImageSection.setVisibility(View.VISIBLE);
                //                mPhotoTitleTv.setText(filemanagerstring);

        }
    }

    fun getPath(uri : Uri) : String {
        var projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor : Cursor = mMainActivity.managedQuery(uri, projection, null, null, null);
            //HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            //THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            val column_index = cursor
                              .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst()
            return cursor.getString(column_index)
    }

    fun setListeners() {
        first_image.setOnClickListener(prepareOnClickListener(SELECT_PICTURE))
        second_image.setOnClickListener(prepareOnClickListener(SELECT_PICTURE_2))
        pierwsza_metoda.setOnClickListener{ performImageTransformation() }
    }

    fun prepareOnClickListener(requestCode: Int) : View.OnClickListener {
        val myListener = View.OnClickListener {
            val intent = Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            this.startActivityForResult(intent, requestCode)
        }
        return  myListener
    }

    fun getBitmapFromUri(uri: Uri?) : Bitmap{
            val parcelFileDescriptor : ParcelFileDescriptor =
                        mMainActivity.contentResolver.openFileDescriptor(uri, "r")
            val fileDescriptor = parcelFileDescriptor.fileDescriptor
                val image = BitmapFactory.decodeFileDescriptor(fileDescriptor)
                parcelFileDescriptor.close()
                return image
            }

    fun convertBitmapToMat(bitmap : Bitmap) : Mat {
        var matrix : Mat = Mat()
        Utils.bitmapToMat(bitmap, matrix)
        return  matrix
    }

    fun convertToGrayscale(matrix : Mat) : Mat? {
        var greyMat : Mat = Mat()
        Imgproc.cvtColor(matrix, greyMat, Imgproc.COLOR_BGR2GRAY)
        return  greyMat
    }

    fun performImageTransformation() {
        progress_bar.visibility = View.VISIBLE
        val mser = com.example.marcin.featuredd.descriptors.MSER()
        mser.INSTANCE.computeFirstImage(convertToGrayscale(convertBitmapToMat(getBitmapFromUri(firstImage))))
        val images : List<Mat> = ArrayList()
        val  matchImage = mser.INSTANCE.matchImages()
        val detectedPoints = mser.INSTANCE.drawDetectedPoints()
        images.plus(matchImage)
        images.plus(detectedPoints)
        eventBus.postSticky(images)
        progress_bar.visibility = View.GONE
        mMainActivity.pushFragments(ResultFragment(), R.id.container, false, true, false)

    }
}