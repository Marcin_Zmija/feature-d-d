package com.example.marcin.featuredd.descriptors

import org.opencv.core.*
import org.opencv.features2d.DescriptorExtractor
import org.opencv.features2d.DescriptorMatcher
import org.opencv.features2d.FeatureDetector
import org.opencv.features2d.Features2d

/**
 * Created by marcin on 08.07.2017.
 */
class MSER() {

    val INSTANCE : MSER = this

    val detector : FeatureDetector = FeatureDetector.create(FeatureDetector.MSER)
    var descriptor = DescriptorExtractor.create(DescriptorExtractor.AKAZE)
    var matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING)

    var mat1 : Mat? = null
    var descriptor1 = Mat()
    var keyPoints1 = MatOfKeyPoint()

    var mat2 : Mat? = null
    var descriptor2 = Mat()
    var keyPoints2 = MatOfKeyPoint()

    var featuredImg = Mat()

    var RED = Scalar(255.0, 0.0, 0.0)
    var GREEN = Scalar(0.0, 255.0, 0.0)
    val kpColor = Scalar(255.0, 159.0, 10.0)

    var outputImg = Mat()
    var drawnMatches = MatOfByte()

    fun computeFirstImage(mat : Mat?) {
        mat1 = mat
        detector.detect(mat1, keyPoints1)
        descriptor.compute(mat1, keyPoints1, descriptor1)
    }

    fun computeSecondImage(mat : Mat?) {
        mat2 = mat
        detector.detect(mat2, keyPoints2)
        descriptor.compute(mat2, keyPoints2, descriptor2)
    }

    fun matchImages() : Mat {
        var matches : MatOfDMatch = MatOfDMatch()
        matcher.match(descriptor1, descriptor2, matches)
        Features2d.drawMatches(mat1, keyPoints1, mat2, keyPoints2, matches,
                outputImg, GREEN, RED,  drawnMatches, Features2d.NOT_DRAW_SINGLE_POINTS)
        return outputImg
    }

    fun drawDetectedPoints() : Mat {
        //this will be color of keypoints
        //featuredImg will be the output of first image
        Features2d.drawKeypoints(mat1, keyPoints1, featuredImg, kpColor, 0)
        return featuredImg
//        //featuredImg will be the output of first image
//        Features2d.drawKeypoints(mat2, keyPoints2, featuredImg, kpColor, 0)
    }

}