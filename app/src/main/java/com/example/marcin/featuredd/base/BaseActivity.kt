package com.example.marcin.featuredd.base

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.example.marcin.featuredd.R


/**
 * Created by user on 10.05.2017.
 */

abstract class BaseActivity : AppCompatActivity() {


    fun pushFragments(fragment: Fragment, content: Int,
                      shouldAnimate: Boolean, backstack: Boolean, revertAnim: Boolean) {
        val manager = supportFragmentManager

        var fragmentPopped = false

        try {
            fragmentPopped = manager.popBackStackImmediate(fragment.javaClass.name, 0)

        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }

        if (!fragmentPopped) {
            val ft = manager.beginTransaction()
            if (shouldAnimate)
                ft.setCustomAnimations(R.anim.slide_in_right,
                        R.anim.slide_out_left, R.anim.slide_in_left,
                        R.anim.slide_out_right)
            if (backstack)
                ft.addToBackStack(fragment.javaClass.name)

            ft.replace(content, fragment, fragment.javaClass.name)
            try {
                ft.commitAllowingStateLoss()
                manager.executePendingTransactions()

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

}
