package com.example.marcin.featuredd.MainView

import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.marcin.featuredd.R
import kotlinx.android.synthetic.main.fragment_result.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.opencv.android.Utils
import org.opencv.core.Mat

/**
 * Created by marcin on 08.08.2017.
 */
class ResultFragment : Fragment() {

    val eventBus : EventBus = EventBus.getDefault()
    var imageList : List<Mat> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_result, container, false)
        return  view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setImages()
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun getPictures(images : List<Mat>)  {
        this.imageList = images
        eventBus.removeStickyEvent(images)
    }

    override fun onResume() {
        super.onResume()
        eventBus.register(this)
    }

    override fun onPause() {
        super.onPause()
        eventBus.unregister(this)
    }

    fun setImages() {
       first_image.setImageBitmap(convertMatToBitmap(imageList[0]))
    }

    fun convertMatToBitmap(mat : Mat) : Bitmap {
        val w = first_image.layoutParams.width
        val h = first_image.layoutParams.height
        val conf = Bitmap.Config.ARGB_8888 // see other conf types
        val bmp = Bitmap.createBitmap(w, h, conf)
        Utils.matToBitmap(mat, bmp)
        return  bmp
    }
}